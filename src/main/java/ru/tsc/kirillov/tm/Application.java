package ru.tsc.kirillov.tm;

import ru.tsc.kirillov.tm.constant.TerminalConst;

public final class Application {

    public static void main(final String[] args) {
        process(args);
    }

    private static void process(final String[] args) {
        if (args == null || args.length == 0)
            return;

        final String firstArg = args[0];
        if (firstArg == null || firstArg.isEmpty())
            return;
        switch (firstArg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showUnexpectedCommand(firstArg);
                break;
        }
    }

    private static void showAbout() {
        showTerminalHeader(TerminalConst.ABOUT);
        System.out.println("Разработчик: Кириллов Максим");
        System.out.println("E-mail: mkirillov@tsconsulting.com");
    }

    private static void showHelp() {
        showTerminalHeader(TerminalConst.HELP);
        System.out.printf("%s - Отображение информации о разработчике.\n", TerminalConst.ABOUT);
        System.out.printf("%s - Отображение версии программы.\n", TerminalConst.VERSION);
        System.out.printf("%s - Отображение доступных команд.\n", TerminalConst.HELP);
    }

    private static void showVersion() {
        showTerminalHeader(TerminalConst.VERSION);
        System.out.println("1.2.0");
    }

    private static void showUnexpectedCommand(final String cmd) {
        showTerminalHeader(TerminalConst.ERROR);
        System.err.printf("Команда `%s` не поддерживается.\n", cmd);
    }

    private static void showTerminalHeader(final String name) {
        System.out.printf("[%s]\n", name != null ? name.toUpperCase() : "N/A");
    }

}
